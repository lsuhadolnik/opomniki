function nastaviHTML(id, val){
	document.getElementById(id).innerHTML = val;
}

function stanjePokrivala(val){
	var el = document.getElementsByClassName("pokrivalo")[0];
	
	if(typeof val == "boolean"){
		el.setAttribute("style","display:"+(val ? "flex" : "none"))
	}
	
	return el.style.display;
}

function el(el){
	return document.createElement(el);	
}

window.addEventListener('load', function() {
	//stran nalozena
		
	
	var mmm = 0;
	var ouch = ["Av!","UFFF!", "FAK!", "O ne!", "JOOJ!"];
		
	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");
		
		console.log(ouch[mmm]);
			if(++mmm >= ouch.length){
				mmm = 0;
			}
		
		for (i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var naziv = opomnik.querySelector(".naziv_opomnika");
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);
	
			if(cas == 0){
				alert(naziv.innerHTML);
				
				opomnik.parentNode.removeChild(opomnik);
			}
			
			else{
				cas--;
				casovnik.innerHTML = cas;
			}
	
			//TODO: 
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	}
	setInterval(posodobiOpomnike, 1000);
	
	var zgodiSeObKlikuNaGumbPotrdi = function(event) {
		
		var el = document.getElementById("uporabnisko_ime");
		var ime = el.value;
		
		alert("Kje si "+ime+"!?");
		
		nastaviHTML("uporabnik", ime);
		stanjePokrivala(false);
		
	}
	
	document.getElementById("prijavniGumb").onclick = zgodiSeObKlikuNaGumbPotrdi;
	
	var dodajOpomnik = function() {
		
		var el = document.getElementById("dodajGumb");
		
		var nazivInput = document.getElementById("naziv_opomnika");
		var casInput = document.getElementById("cas_opomnika");
		
		var naziv = nazivInput.value;
		var cas   = casInput.value;
		
		nazivInput.value = null;
		casInput.value   = null;
		
		var opomniki = document.getElementById("opomniki");
		
		var html = 
			"<div class='opomnik'>" +
					"<div class='naziv_opomnika'>" +
						naziv +
					"</div>" + 
				"<div class='cas_opomnika'>" + 
					"Opomnik čez "+ 
						"<span>" +
							cas +
						"</span> " +
					"sekund."+
				"</div>" +
			"</div>"
			
		opomniki.innerHTML = opomniki.innerHTML + html;
		
		
	}
	
	document.getElementById("dodajGumb").onclick = dodajOpomnik;
	
});